package org.home.library;

import org.home.dao.entity.Authors;
import org.home.dao.entity.Book;
import org.home.dao.entity.Genre;
import org.home.dao.entity.SearchCriteria;
import org.home.dao.service.BookService;
import org.home.model.ListLibAttr;
import org.home.model.LibraryResponse;
import org.home.model.LibraryData;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
public class LibraryController {
    private final int limit = 5;

    @RequestMapping(value = "lib/range", method = { RequestMethod.POST })
    @ResponseStatus(value = HttpStatus.OK)
    @Secured({ "ROLE_ADMIN", "ROLE_USER" })
    public List<Book> listBooksByRangeDefault() {
        BookService bookService = new BookService();
        return bookService.getAllBooks();
    }

    @RequestMapping(value = "lib/range/{limit}/{offset}", method = { RequestMethod.POST })
    @ResponseStatus(value = HttpStatus.OK)
    @Secured({ "ROLE_ADMIN", "ROLE_USER" })
    public LibraryResponse listBooksByRange(@PathVariable(value = "limit") Integer limit, @PathVariable(value = "offset") Integer offset, SecurityContextHolder contextHolder) {
        return new LibraryData(contextHolder).getRangeBooks(limit, offset);
    }

    @RequestMapping(value = "lib/range/{tab}", method = { RequestMethod.POST })
    @ResponseStatus(value = HttpStatus.OK)
    @Secured({ "ROLE_ADMIN", "ROLE_USER" })
    public LibraryResponse listBooksPaginator(@PathVariable(value = "tab") Integer tab, SecurityContextHolder contextHolder) {
        return new LibraryData(contextHolder).getRangeBooks(limit, tab);
    }

    @RequestMapping(value = "lib/edit/{tab}", method = { RequestMethod.POST })
    @ResponseStatus(value = HttpStatus.OK)
    @Secured({ "ROLE_ADMIN" })
    public LibraryResponse editBook(@PathVariable(value = "tab") Integer tab, @RequestBody Book book, SecurityContextHolder contextHolder) {
        return new LibraryData(contextHolder).updateBook(book, limit, tab);
    }

    @RequestMapping(value = "lib/add", method = { RequestMethod.POST })
    @ResponseStatus(value = HttpStatus.OK)
    @Secured({ "ROLE_ADMIN" })
    public LibraryResponse addBook(@RequestBody Book book, SecurityContextHolder contextHolder) {
        return new LibraryData(contextHolder).insertBook(book, limit);
    }

    @RequestMapping(value = "lib/genre/{gId}/{tagId}", method = { RequestMethod.POST })
    @ResponseStatus(value = HttpStatus.OK)
    @Secured({ "ROLE_ADMIN", "ROLE_USER" })
    public ListLibAttr<Genre> getGenreBook(@PathVariable(value = "gId") Integer gId, @PathVariable(value = "tagId") String tagId, SecurityContextHolder contextHolder) {
        return new LibraryData(contextHolder).getGenres(gId, tagId);
    }

    @RequestMapping(value = "lib/authors/{aId}/{tagId}", method = { RequestMethod.POST })
    @ResponseStatus(value = HttpStatus.OK)
    @Secured({ "ROLE_ADMIN", "ROLE_USER" })
    public ListLibAttr<Authors> getAuthorsBook(@PathVariable(value = "aId") Integer aId, @PathVariable(value = "tagId") String tagId, SecurityContextHolder contextHolder) {
        return new LibraryData(contextHolder).getAuthors(aId, tagId);
    }

    @RequestMapping(value = "lib/delete/{id}", method = { RequestMethod.DELETE })
    @ResponseStatus(value = HttpStatus.OK)
    @Secured({ "ROLE_ADMIN" })
    public LibraryResponse editBook(@PathVariable(value = "id") Integer id, SecurityContextHolder contextHolder) {
        return new LibraryData(contextHolder).deleteBook(id, limit, 1);
    }

    @RequestMapping(value = "lib/search", method = { RequestMethod.POST })
    @ResponseStatus(value = HttpStatus.OK)
    @Secured({ "ROLE_ADMIN", "ROLE_USER" })
    public LibraryResponse searchBook(@RequestBody SearchCriteria searchCriteria, SecurityContextHolder contextHolder) {
        return new LibraryData(contextHolder).searchBook(searchCriteria, limit);
    }

    @RequestMapping(value = "lib/search/{tab}/{searchId}", method = { RequestMethod.POST })
    @ResponseStatus(value = HttpStatus.OK)
    @Secured({ "ROLE_ADMIN", "ROLE_USER" })
    public LibraryResponse searchBookById(@PathVariable(value = "tab") Integer tab, @PathVariable(value = "searchId") Integer searchId, SecurityContextHolder contextHolder) {
        return new LibraryData(contextHolder).searchBook(limit, tab, searchId);
    }




}
