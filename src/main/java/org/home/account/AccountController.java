package org.home.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
public class AccountController {

    @Autowired
    private AccountService accountService;


    @GetMapping("account/current")
    @ResponseStatus(value = HttpStatus.OK)
    @Secured({ "ROLE_ADMIN", "ROLE_USER" })
    public UserDetails currentAccount(Principal principal) {
        Assert.notNull(principal);
        return accountService.loadUserByUsername(principal.getName());
    }

}
