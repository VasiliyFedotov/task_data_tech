package org.home.account;

import java.util.Collections;

import org.home.dao.service.UserService;
import org.home.utils.AccountRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.*;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.*;


import org.springframework.stereotype.Service;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class AccountService implements UserDetailsService {
	
	@Autowired
	private UserService userService;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		org.home.dao.entity.User user = userService.getUserByLogin(username);
		if(user == null) {
			throw new UsernameNotFoundException("user not found");
		}
		return createUser(user);
	}
	
	public void signin(org.home.dao.entity.User user) {
		SecurityContextHolder.getContext().setAuthentication(authenticate(user));
	}
	private Authentication authenticate(org.home.dao.entity.User user) {
		return new UsernamePasswordAuthenticationToken(createUser(user), null, Collections.singleton(createAuthority(user)));
	}

	private User createUser(org.home.dao.entity.User user) {
		return new User(user.getLogin(), user.getPassword(), Collections.singleton(createAuthority(user)));
	}

	private GrantedAuthority createAuthority(org.home.dao.entity.User user) {
		AccountRole role = AccountRole.values()[user.getRole()];
		return new SimpleGrantedAuthority(role.name());
	}

}
