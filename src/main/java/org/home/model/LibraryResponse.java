package org.home.model;

import org.home.dao.entity.Book;

import java.util.List;

public class LibraryResponse {

    private List<Book> library;
    private Integer limit;
    private Integer activePagingTab;
    private Integer totalPagingTab;
    private Integer searchId;
    private boolean adminRole;

    public List<Book> getLibrary() {
        return library;
    }

    public void setLibrary(List<Book> library) {
        this.library = library;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getActivePagingTab() {
        return activePagingTab;
    }

    public void setActivePagingTab(Integer activePagingTab) {
        this.activePagingTab = activePagingTab;
    }

    public Integer getTotalPagingTab() {
        return totalPagingTab;
    }

    public void setTotalPagingTab(Integer totalDocuments) {
        this.totalPagingTab = totalDocuments % limit > 0 ? totalDocuments / limit + 1 : totalDocuments / limit;
    }

    public Integer getOffset() {
        return (activePagingTab - 1) * limit;
    }

    public Integer getSearchId() {
        return searchId;
    }

    public void setSearchId(Integer searchId) {
        this.searchId = searchId;
    }

    public boolean isAdminRole() {
        return adminRole;
    }

    public void setAdminRole(boolean adminRole) {
        this.adminRole = adminRole;
    }
}
