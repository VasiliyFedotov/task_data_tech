package org.home.model;

import org.home.dao.entity.Authors;
import org.home.dao.entity.Book;
import org.home.dao.entity.Genre;
import org.home.dao.entity.SearchCriteria;
import org.home.dao.service.*;
import org.home.utils.AccountRole;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Collection;

public class LibraryData {
    private SecurityContextHolder contextHolder;

    public LibraryData(SecurityContextHolder contextHolder) {
        this.contextHolder = contextHolder;
    }

    public LibraryResponse getRangeBooks(Integer limit, Integer pagingTab) {
        LibraryResponse response = initRespObject(limit, pagingTab);

        BookService bookService = new BookService();
        CounterService counterService = new CounterService();

        response.setLibrary(bookService.getBooksByRangeWithAuthor(response.getLimit(), response.getOffset()));
        response.setTotalPagingTab(counterService.getTotalBooks());
        response.setAdminRole(isAdmin(contextHolder));
        return response;
    }

    public ListLibAttr<Genre> getGenres(int id, String tagId) {
        ListLibAttr<Genre> response = new ListLibAttr<>();
        GenreService genreService = new GenreService();
        response.setAttributes(genreService.getAllGenres());
        return initResponse(id, tagId, response);
    }

    public ListLibAttr<Authors> getAuthors(int id, String tagId) {
        ListLibAttr<Authors> response = new ListLibAttr<>();
        AuthorService authorService = new AuthorService();
        response.setAttributes(authorService.getAllAuthors());
        return initResponse(id, tagId, response);
    }

    public LibraryResponse updateBook(Book book, Integer limit, Integer pagingTab) {
        BookService bookService = new BookService();
        bookService.updateBook(book);
        return getRangeBooks(limit, pagingTab);
    }

    public LibraryResponse deleteBook(Integer id, Integer limit, Integer pagingTab) {
        BookService bookService = new BookService();
        bookService.deleteBook(id);
        return getRangeBooks(limit, pagingTab);
    }

    public LibraryResponse insertBook(Book book, Integer limit) {
        BookService bookService = new BookService();
        bookService.insertBook(book);
        CounterService counterService = new CounterService();
        LibraryResponse resp = initRespObject(limit, 1);
        resp.setTotalPagingTab(counterService.getTotalBooks());
        return getRangeBooks(limit, resp.getTotalPagingTab());
    }

    public LibraryResponse searchBook(SearchCriteria searchCriteria, Integer limit) {
        LibraryResponse response = initRespObject(limit, 1);

        SearchCriteriaService scService = new SearchCriteriaService();
        scService.setSearchCriteria(searchCriteria);
        response.setSearchId(scService.getLastSearchCriteria());

        BookService bookService = new BookService();
        response.setLibrary(bookService.getSearchBooksByRangeWithAuthor(limit, response.getOffset(), searchCriteria));
        response.setTotalPagingTab(bookService.getSearchCount(searchCriteria));
        response.setAdminRole(isAdmin(contextHolder));
        return response;
    }

    public LibraryResponse searchBook(Integer limit, Integer tab, Integer searchId) {
        LibraryResponse response = initRespObject(limit, tab);

        SearchCriteriaService scService = new SearchCriteriaService();
        SearchCriteria searchCriteria = scService.getSearchCriteriaById(searchId);
        response.setSearchId(searchId);

        BookService bookService = new BookService();
        response.setLibrary(bookService.getSearchBooksByRangeWithAuthor(limit, response.getOffset(), searchCriteria));
        response.setTotalPagingTab(bookService.getSearchCount(searchCriteria));
        response.setAdminRole(isAdmin(contextHolder));
        return response;
    }

    private <T> ListLibAttr<T> initResponse(int id, String tagId, ListLibAttr<T> response) {
        response.setActionId(id);
        response.setTagId(tagId);
        return response;
    }

    private LibraryResponse initRespObject(Integer limit, Integer pagingTab) {
        LibraryResponse lrObject = new LibraryResponse();
        lrObject.setLimit(limit);
        lrObject.setActivePagingTab(pagingTab);
        return lrObject;
    }

    private Boolean isAdmin(SecurityContextHolder contextHolder) {
        Authentication authentication = contextHolder.getContext().getAuthentication();
        if (authentication.isAuthenticated()) {
            Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
            return authorities.contains(new SimpleGrantedAuthority(AccountRole.ROLE_ADMIN.name()));
        }
        return null;
    }

}
