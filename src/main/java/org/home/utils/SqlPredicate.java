package org.home.utils;

public enum SqlPredicate {
    AND,
    OR,
    NOT
}
