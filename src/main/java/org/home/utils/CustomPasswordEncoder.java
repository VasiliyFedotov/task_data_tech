package org.home.utils;

import org.springframework.security.crypto.password.PasswordEncoder;

import java.io.Serializable;
import java.security.MessageDigest;

public class CustomPasswordEncoder implements Serializable, PasswordEncoder {
	private String algorithm;

	public CustomPasswordEncoder(){
		this.algorithm = "md5";
	}

	public String encode(String password){
		return encode(password, algorithm);
	}

	public String encode(String password, String algorithm){
		byte[] digest = null;
		if(password != null && password.length() > 0) {
			try {
				MessageDigest md = MessageDigest.getInstance(algorithm);
				md.update(password.getBytes());
				digest = md.digest();
			} catch(Exception e){
				//LOG
			}

			StringBuffer sb = new StringBuffer();
			for (byte b : digest) {
				sb.append(String.format("%02x", b & 0xff));
			}
			return sb.toString();
		}
		return null;
	}

	@Override
	public String encode(CharSequence rawPassword) {
		return encode(rawPassword.toString());
	}

	@Override
	public boolean matches(CharSequence rawPassword, String encodedPassword) {
		return encode(rawPassword.toString()).equals(encodedPassword);
	}
}