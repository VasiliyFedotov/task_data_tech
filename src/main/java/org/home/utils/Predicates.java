package org.home.utils;

import java.util.ArrayList;
import java.util.List;

public class Predicates {
    private final String BEGIN = " WHERE ";
    private final String AND = " and ";
    private final String OR = " or ";
    private final String NOT = " not ";

    private List<String> sb;

    public Predicates(){
        sb = new ArrayList<>();
    }

    private void addSqlPredicate(SqlPredicate sqlPredicate){
        if (sb.size() != 0){
            switch (sqlPredicate) {
                case OR:
                    sb.add(OR);
                    break;
                case AND:
                    sb.add(AND);
                    break;
                case NOT:
                    sb.add(NOT);
                    break;
            }
        } else
            sb.add(BEGIN);
    }
    private <T> boolean checkParams(SqlPredicate sqlPredicate, String fieldName, T searchParam){
        if(sqlPredicate == null)
            return true;
        if(fieldName == null)
            return true;
        if(searchParam != null){
            if(searchParam instanceof String)
                return ((String) searchParam).length() == 0;
            if(searchParam instanceof Integer)
                return ((Integer) searchParam).intValue() == 0;
        }
        return true;
    }
    public String build(){
        String result = "";
        for (String item : sb) {
            result += item;
        }
        return result;
    }
    public void setSearchParam(SqlPredicate sqlPredicate, String fieldName, Integer searchParam) {
        if (checkParams(sqlPredicate, fieldName ,searchParam))
            return;
        addSqlPredicate(sqlPredicate);
        sb.add(fieldName + "=" + searchParam);
    }

    public void setSearchParamByLike(SqlPredicate sqlPredicate, String fieldName, String searchParam) {
        if (checkParams(sqlPredicate, fieldName ,searchParam))
            return;
        addSqlPredicate(sqlPredicate);
        sb.add(fieldName + " like ");
        sb.add("%'" + searchParam + "'%");
    }
    public void setSearchParam(SqlPredicate sqlPredicate, String fieldName, String searchParam) {
        if (checkParams(sqlPredicate, fieldName ,searchParam))
            return;
        addSqlPredicate(sqlPredicate);
        sb.add(fieldName + "='" + searchParam + "'");
    }
}
