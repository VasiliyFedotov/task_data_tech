package org.home.utils;

public enum AccountRole {
    ROLE_ADMIN,
    ROLE_MODERATOR,
    ROLE_USER
}
