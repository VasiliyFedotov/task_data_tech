package org.home.dao.mappers;

import org.apache.ibatis.annotations.*;
import org.home.dao.entity.Author;
import org.home.dao.entity.Authors;
import org.home.dao.typehandlers.ArrayTypeHandler;

import java.util.List;

public interface AuthorMapper {

    @Select("SELECT  * FROM examination.author WHERE id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "groupId", column = "group_id"),
            @Result(property = "firstname", column = "firstname"),
            @Result(property = "secondname", column = "secondname"),
            @Result(property = "surname", column = "surname"),
            @Result(property = "isAlias", column = "is_alias"),
            @Result(property = "parentId", column = "parent_id")
    })
    public Author getAuthorById(Integer id);

    @Select("SELECT  * FROM examination.author WHERE firstname = #{firstname}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "groupId", column = "group_id"),
            @Result(property = "firstname", column = "firstname"),
            @Result(property = "secondname", column = "secondname"),
            @Result(property = "surname", column = "surname"),
            @Result(property = "isAlias", column = "is_alias"),
            @Result(property = "parentId", column = "parent_id")
    })
    public List<Author> getAuthorByFirstname(String firstname);

    @Select("SELECT  * FROM examination.author WHERE secondname = #{secondname}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "groupId", column = "group_id"),
            @Result(property = "firstname", column = "firstname"),
            @Result(property = "secondname", column = "secondname"),
            @Result(property = "surname", column = "surname"),
            @Result(property = "isAlias", column = "is_alias"),
            @Result(property = "parentId", column = "parent_id")
    })
    public List<Author> getAuthorBySecondname(String secondname);

    @Select("SELECT  * FROM examination.author WHERE surname = #{surname}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "groupId", column = "group_id"),
            @Result(property = "firstname", column = "firstname"),
            @Result(property = "secondname", column = "secondname"),
            @Result(property = "surname", column = "surname"),
            @Result(property = "isAlias", column = "is_alias"),
            @Result(property = "parentId", column = "parent_id")
    })
    public List<Author> getAuthorBySurname(String surname);


    @Select("select group_id, array_agg(concat(firstname, ' ', surname)) as authors from examination.author GROUP BY group_id order by group_id")
    @Results({
            @Result(property = "id", column = "group_id"),
            @Result(property = "name", column = "authors", typeHandler = ArrayTypeHandler.class),
    })
    public List<Authors> getAllAuthors();
}
