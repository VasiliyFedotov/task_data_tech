package org.home.dao.mappers;

import org.apache.ibatis.annotations.*;
import org.home.dao.entity.SearchCriteria;

public interface SearchCriteriaMapper {

    @Select("SELECT * FROM examination.search_criteria WHERE id = #{id}")
    @Results({
        @Result(property = "id", column = "id"),
        @Result(property = "title", column = "sc_title"),
        @Result(property = "description", column = "sc_description"),
        @Result(property = "genreId", column = "genre_id"),
        @Result(property = "authorGroupId", column = "sc_author_group_id")
    })
    public SearchCriteria getSearchCriteriaById(Integer id);

    @Select("SELECT max(id) FROM examination.search_criteria")
    public Integer getLastSearchCriteria();

    @Insert("insert into examination.search_criteria(sc_title, sc_description, sc_genre_id, sc_author_group_id) values(#{title}, #{description}, #{genreId}, #{authorGroupId})")
    public void setSearchCriteria(SearchCriteria sc);
}
