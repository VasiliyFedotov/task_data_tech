package org.home.dao.mappers;

import org.apache.ibatis.annotations.*;
import org.home.dao.entity.Genre;

import java.util.List;

public interface GenreMapper {

    @Select("SELECT  * FROM examination.genre WHERE id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "text")
    })
    public Genre getGenreById(Integer id);

    @Select("SELECT  * FROM examination.genre WHERE text = #{name}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "text")
    })
    public Genre getGenreByName(String name);

    @Select("SELECT * FROM examination.genre")
    @Results({
            @Result(id = true, property = "id", column = "id"),
            @Result(property = "name", column = "text"),
    })
    public List<Genre> getAllGenres();
}
