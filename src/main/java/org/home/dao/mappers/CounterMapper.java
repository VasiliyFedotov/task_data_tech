package org.home.dao.mappers;

import org.apache.ibatis.annotations.Select;


public interface CounterMapper {

    @Select("SELECT  count(*) FROM examination.book")
    public Integer getTotalBooks();
}
