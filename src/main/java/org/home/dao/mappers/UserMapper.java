package org.home.dao.mappers;

import org.apache.ibatis.annotations.*;
import java.util.List;
import java.util.UUID;

import org.home.dao.entity.User;
import org.home.dao.typehandlers.UUIDTypeHandler;


public interface UserMapper {
//    TODO
//    @Insert("INSERT INTO examination.users(user_id, name, login, password, algorithm, role) VALUES(#{userId}, #{name}, #{login}, #{password}, #{algorithm}, #{role})")
//    @Options(useGeneratedKeys = true, keyProperty = "id")
//    public void insertUser(User user);
    @Select("SELECT  * FROM examination.users WHERE user_id = #{userId}")
    public User getUserByUserId(@Param("userId") UUID userId);

    @Select("SELECT  * FROM examination.users WHERE name = #{name}")
    public User getUserByName(String name);

    @Select("SELECT  * FROM examination.users WHERE login = #{login}")
    public User getUserByLogin(String login);

    @Select("SELECT * FROM examination.users")
    @Results({
            @Result(id = true, property = "id", column = "id"),
            @Result(property = "userId", column = "user_id", typeHandler = UUIDTypeHandler.class),
            @Result(property = "name", column = "name"),
            @Result(property = "login", column = "login"),
            @Result(property = "password", column = "password"),
            @Result(property = "algorithm", column = "algorithm"),
            @Result(property = "role", column = "role")
    })
    public List<User> getAllUsers();

//    TODO
//    @Update("UPDATE examination.users SET ...")
//    public void updateUser(User user);

    @Delete("DELETE FROM examination.users WHERE user_id=#{userId}")
    public void deleteUser(@Param("userId") UUID userId);

}
