package org.home.dao.mappers;

import org.apache.ibatis.annotations.*;
import org.home.dao.entity.Book;
import org.home.dao.typehandlers.ArrayTypeHandler;

import java.util.List;

public interface BookMapper {

    @Select("SELECT  * FROM examination.book WHERE id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "description", column = "description"),
            @Result(property = "genreId", column = "genre_id"),
            @Result(property = "authorGroupId", column = "author_group_id")
    })
    public Book getBookById(Integer id);

    @Select("SELECT  * FROM examination.book")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "description", column = "description"),
            @Result(property = "genreId", column = "genre_id"),
            @Result(property = "authorGroupId", column = "author_group_id")
    })
    public List<Book> getAllBooks();

    @Select("SELECT  * FROM examination.book order by id limit #{limit} offset #{offset}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "description", column = "description"),
            @Result(property = "genreId", column = "genre_id"),
            @Result(property = "authorGroupId", column = "author_group_id")
    })
    public List<Book> getRangeBooks(@Param("limit")Integer limit, @Param("offset")Integer offset);

    @Select("select alfa.id as id, alfa.title as title, alfa.description as description, alfa.genre_id as genre_id, betta.text as genre, alfa.author_group_id as author_group_id, omega.author as author from examination.book alfa, examination.genre betta, (select group_id, array_agg(concat(firstname, ' ', surname)) as author from examination.author GROUP BY group_id) omega where alfa.genre_id = betta.id and alfa.author_group_id = omega.group_id order by id limit #{limit} offset #{offset}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "description", column = "description"),
            @Result(property = "genreId", column = "genre_id"),
            @Result(property = "authorGroupId", column = "author_group_id"),
            @Result(property = "genre", column = "genre"),
            @Result(property = "authorGroup", column = "author", typeHandler = ArrayTypeHandler.class)
    })
    public List<Book> getBooksByRangeWithAuthor(@Param("limit")Integer limit, @Param("offset")Integer offset);

    @Select("select alfa.id as id, alfa.title as title, alfa.description as description, alfa.genre_id as genre_id, betta.text as genre, alfa.author_group_id as author_group_id, omega.author as author " +
            "from (select * from examination.book ${search}) alfa, examination.genre betta, " +
            "(select group_id, array_agg(concat(firstname, ' ', surname)) as author from examination.author GROUP BY group_id) omega " +
            "where alfa.genre_id = betta.id and alfa.author_group_id = omega.group_id order by id limit #{limit} offset #{offset}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "description", column = "description"),
            @Result(property = "genreId", column = "genre_id"),
            @Result(property = "authorGroupId", column = "author_group_id"),
            @Result(property = "genre", column = "genre"),
            @Result(property = "authorGroup", column = "author", typeHandler = ArrayTypeHandler.class)
    })
    public List<Book> getSearchBooksByRangeWithAuthor(@Param("limit")Integer limit, @Param("offset")Integer offset, @Param("search")String search);

    @Select("select count(*) from examination.book ${search}")
    public Integer getSearchCount(@Param("search")String search);

    @Update("update examination.book set title = #{title}, description= #{description}, genre_id=#{genreId}, author_group_id=#{authorGroupId} where id=#{id}")
    public void updateBook(Book book);

    @Delete("delete from examination.book where id=#{id}")
    public void deleteBook(Integer id);

    @Insert("insert into examination.book (title, description, genre_id, author_group_id) values(#{title}, #{description}, #{genreId}, #{authorGroupId})")
    public void insertBook(Book book);

}
