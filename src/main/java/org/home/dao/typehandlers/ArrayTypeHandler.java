package org.home.dao.typehandlers;

import java.sql.*;
import java.util.*;
import org.apache.ibatis.type.*;

/**
 * Handler for List types.
 *
 * @see List
 */

public class ArrayTypeHandler<T> implements TypeHandler<List<T>> {

    @Override
    public void setParameter(PreparedStatement ps, int i, List<T> parameter, JdbcType jdbcType) throws SQLException {
        if (parameter == null) {
            ps.setObject(i, null, Types.ARRAY);
        } else {
            ps.setObject(i, parameter.toString(), Types.ARRAY);
        }
    }

    @Override
    public List<T> getResult(ResultSet rs, String columnName) throws SQLException {
        return convert(rs.getArray(columnName));
    }

    @Override
    public List<T> getResult(ResultSet rs, int columnIndex) throws SQLException {
        return convert(rs.getArray(columnIndex));
    }

    @Override
    public List<T> getResult(CallableStatement cs, int columnIndex) throws SQLException {
        return convert(cs.getArray(columnIndex));
    }

    private List<T> convert(Array array) throws SQLException{
        return Arrays.asList((T[])array.getArray());
    }
}
