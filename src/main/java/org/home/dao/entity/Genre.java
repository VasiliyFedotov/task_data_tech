package org.home.dao.entity;

public class Genre {
    private Integer id;
    private String name;

    @Override
    public String toString() {
        return String.format("Genre [id: %s, name: %s]", id, name);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
