package org.home.dao.entity;

public class SearchCriteria {

    private Integer id;
    private String title;
    private String description;
    private Integer genreId;
    private Integer authorGroupId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getGenreId() {
        return genreId;
    }

    public void setGenreId(Integer genreId) {
        this.genreId = genreId;
    }

    public Integer getAuthorGroupId() {
        return authorGroupId;
    }

    public void setAuthorGroupId(Integer authorGroupId) {
        this.authorGroupId = authorGroupId;
    }
}
