package org.home.dao.entity;

import org.home.utils.AccountRole;

import java.util.UUID;

public class User {
    private Integer id;
    private UUID userId;
    private String name;
    private String login;
    private String password;
    private String algorithm;
    private Integer role;

    @Override
    public String toString(){
        return String.format("User [UUID: %s, name: %s, role: %s]",userId, name, AccountRole.values()[role]);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }
}
