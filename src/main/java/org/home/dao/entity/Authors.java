package org.home.dao.entity;

import java.util.List;

public class Authors {
    private Integer id;
    private List<String> name;

    @Override
    public String toString() {
        return String.format("Authors [id: %s, names: %s]", id, expendList());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return expendList();
    }

    public void setName(List<String> name) {
        this.name = name;
    }

    private String expendList(){
        if(name != null){
            StringBuilder sb = new StringBuilder();
            for (String n : name) {
                if (sb.length() > 0)
                    sb.append(", ");
                sb.append(n);
            }
            return sb.toString();
        }
        return null;
    }
}
