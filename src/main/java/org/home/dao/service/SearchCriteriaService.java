package org.home.dao.service;

import org.apache.ibatis.session.SqlSession;
import org.home.dao.entity.SearchCriteria;
import org.home.dao.mappers.SearchCriteriaMapper;


public class SearchCriteriaService {

    public SearchCriteria getSearchCriteriaById(Integer id) {
        SqlSession sqlSession = BatisSessionFactory.getFactory().openSession();
        try {
            SearchCriteriaMapper searchCriteriaMapper = sqlSession.getMapper(SearchCriteriaMapper.class);
            return searchCriteriaMapper.getSearchCriteriaById(id);
        } finally {
            sqlSession.close();
        }
    }

    public Integer getLastSearchCriteria() {
        SqlSession sqlSession = BatisSessionFactory.getFactory().openSession();
        try {
            SearchCriteriaMapper searchCriteriaMapper = sqlSession.getMapper(SearchCriteriaMapper.class);
            return searchCriteriaMapper.getLastSearchCriteria();
        } finally {
            sqlSession.close();
        }
    }

    public void setSearchCriteria(SearchCriteria searchCriteria) {
        SqlSession sqlSession = BatisSessionFactory.getFactory().openSession();
        try {
            SearchCriteriaMapper searchCriteriaMapper = sqlSession.getMapper(SearchCriteriaMapper.class);
            searchCriteriaMapper.setSearchCriteria(searchCriteria);
            sqlSession.commit();
        } finally {
            sqlSession.close();
        }
    }
}
