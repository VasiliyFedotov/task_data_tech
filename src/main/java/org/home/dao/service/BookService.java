package org.home.dao.service;

import org.apache.ibatis.session.SqlSession;
import org.home.dao.entity.Book;
import org.home.dao.entity.SearchCriteria;
import org.home.dao.mappers.BookMapper;
import org.home.utils.Predicates;
import org.home.utils.SqlPredicate;

import java.util.List;

public class BookService {

    public Book getBookById(Integer id){
        SqlSession sqlSession = BatisSessionFactory.getFactory().openSession();
        try{
            BookMapper bookMapper = sqlSession.getMapper(BookMapper.class);
            return bookMapper.getBookById(id);
        }finally {
            sqlSession.close();
        }
    }

    public List<Book> getAllBooks(){
        SqlSession sqlSession = BatisSessionFactory.getFactory().openSession();
        try{
            BookMapper bookMapper = sqlSession.getMapper(BookMapper.class);
            return bookMapper.getAllBooks();
        }finally {
            sqlSession.close();
        }
    }

    public List<Book> getRangeBooks(Integer limit, Integer offset){
        SqlSession sqlSession = BatisSessionFactory.getFactory().openSession();
        try{
            BookMapper bookMapper = sqlSession.getMapper(BookMapper.class);
            return bookMapper.getRangeBooks(limit, offset);
        }finally {
            sqlSession.close();
        }
    }

    public List<Book> getBooksByRangeWithAuthor(Integer limit, Integer offset){
        SqlSession sqlSession = BatisSessionFactory.getFactory().openSession();
        try{
            BookMapper bookMapper = sqlSession.getMapper(BookMapper.class);
            return bookMapper.getBooksByRangeWithAuthor(limit, offset);
        }finally {
            sqlSession.close();
        }
    }

    public Integer getSearchCount(SearchCriteria searchCriteria){
        SqlSession sqlSession = BatisSessionFactory.getFactory().openSession();
        try{
            BookMapper bookMapper = sqlSession.getMapper(BookMapper.class);
            Predicates predicates = new Predicates();
            predicates.setSearchParam(SqlPredicate.AND, "title", searchCriteria.getTitle());
            predicates.setSearchParamByLike(SqlPredicate.AND, "description", searchCriteria.getDescription());
            predicates.setSearchParam(SqlPredicate.AND, "genre_id", searchCriteria.getGenreId());
            predicates.setSearchParam(SqlPredicate.AND, "author_group_id", searchCriteria.getAuthorGroupId());

            return bookMapper.getSearchCount(predicates.build());
        }finally {
            sqlSession.close();
        }
    }

    public List<Book> getSearchBooksByRangeWithAuthor(Integer limit, Integer offset, SearchCriteria searchCriteria){
        SqlSession sqlSession = BatisSessionFactory.getFactory().openSession();
        try{
            BookMapper bookMapper = sqlSession.getMapper(BookMapper.class);
            Predicates predicates = new Predicates();
            predicates.setSearchParam(SqlPredicate.AND, "title", searchCriteria.getTitle());
            predicates.setSearchParamByLike(SqlPredicate.AND, "description", searchCriteria.getDescription());
            predicates.setSearchParam(SqlPredicate.AND, "genre_id", searchCriteria.getGenreId());
            predicates.setSearchParam(SqlPredicate.AND, "author_group_id", searchCriteria.getAuthorGroupId());
            return bookMapper.getSearchBooksByRangeWithAuthor(limit, offset, predicates.build());
        }finally {
            sqlSession.close();
        }
    }

    public void updateBook(Book book){
        SqlSession sqlSession = BatisSessionFactory.getFactory().openSession();
        try{
            BookMapper bookMapper = sqlSession.getMapper(BookMapper.class);
            bookMapper.updateBook(book);
            sqlSession.commit();
        }finally {
            sqlSession.close();
        }
    }

    public void deleteBook(Integer bookId){
        SqlSession sqlSession = BatisSessionFactory.getFactory().openSession();
        try{
            BookMapper bookMapper = sqlSession.getMapper(BookMapper.class);
            bookMapper.deleteBook(bookId);
            sqlSession.commit();
        }finally {
            sqlSession.close();
        }
    }
    public void insertBook(Book book){
        SqlSession sqlSession = BatisSessionFactory.getFactory().openSession();
        try{
            BookMapper bookMapper = sqlSession.getMapper(BookMapper.class);
            bookMapper.insertBook(book);
            sqlSession.commit();
        }finally {
            sqlSession.close();
        }
    }
}
