package org.home.dao.service;

import org.apache.ibatis.session.SqlSession;
import org.home.dao.mappers.CounterMapper;

public class CounterService {

    public Integer getTotalBooks(){
        SqlSession sqlSession = BatisSessionFactory.getFactory().openSession();
        try{
            CounterMapper counterMapper = sqlSession.getMapper(CounterMapper.class);
            return counterMapper.getTotalBooks();
        }finally {
            sqlSession.close();
        }
    }
}
