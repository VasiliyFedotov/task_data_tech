package org.home.dao.service;


import org.apache.ibatis.session.*;
import org.apache.ibatis.io.Resources;



import java.io.IOException;
import java.io.Reader;

public class BatisSessionFactory {
    private static SqlSessionFactory factory;
    private BatisSessionFactory(){}

    static{
        Reader reader = null;
        try{
            reader = Resources.getResourceAsReader("mybatis-config.xml");
        } catch (IOException ioe){
            throw new RuntimeException(ioe.getMessage());
        }
        factory = new SqlSessionFactoryBuilder().build(reader);
    }

    public static SqlSessionFactory getFactory(){
        return factory;
    }
}
