package org.home.dao.service;

import org.apache.ibatis.session.SqlSession;
import org.home.dao.entity.Author;
import org.home.dao.entity.Authors;
import org.home.dao.mappers.AuthorMapper;

import java.util.List;

public class AuthorService {

    public Author getAuthorById(Integer id){
        SqlSession sqlSession = BatisSessionFactory.getFactory().openSession();
        try{
            AuthorMapper authorMapper = sqlSession.getMapper(AuthorMapper.class);
            return authorMapper.getAuthorById(id);
        }finally {
            sqlSession.close();
        }
    }
    public List<Author> getAuthorByFirstname(String firstname){
        SqlSession sqlSession = BatisSessionFactory.getFactory().openSession();
        try{
            AuthorMapper authorMapper = sqlSession.getMapper(AuthorMapper.class);
            return authorMapper.getAuthorByFirstname(firstname);
        }finally {
            sqlSession.close();
        }
    }
    public List<Author> getAuthorBySecondname(String secondname){
        SqlSession sqlSession = BatisSessionFactory.getFactory().openSession();
        try{
            AuthorMapper authorMapper = sqlSession.getMapper(AuthorMapper.class);
            return authorMapper.getAuthorBySecondname(secondname);
        }finally {
            sqlSession.close();
        }
    }
    public List<Author> getAuthorBySurname(String surname){
        SqlSession sqlSession = BatisSessionFactory.getFactory().openSession();
        try{
            AuthorMapper authorMapper = sqlSession.getMapper(AuthorMapper.class);
            return authorMapper.getAuthorBySurname(surname);
        }finally {
            sqlSession.close();
        }
    }
    public List<Authors> getAllAuthors(){
        SqlSession sqlSession = BatisSessionFactory.getFactory().openSession();
        try{
            AuthorMapper authorMapper = sqlSession.getMapper(AuthorMapper.class);
            return authorMapper.getAllAuthors();
        }finally {
            sqlSession.close();
        }
    }
}
