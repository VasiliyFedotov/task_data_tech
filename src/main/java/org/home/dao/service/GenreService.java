package org.home.dao.service;

import org.apache.ibatis.session.SqlSession;
import org.home.dao.entity.Genre;
import org.home.dao.mappers.GenreMapper;

import java.util.List;

public class GenreService {

    public Genre getGenreById(Integer id){
        SqlSession sqlSession = BatisSessionFactory.getFactory().openSession();
        try{
            GenreMapper genreMapper = sqlSession.getMapper(GenreMapper.class);
            return genreMapper.getGenreById(id);
        }finally {
            sqlSession.close();
        }
    }

    public Genre getGenreByName(String name){
        SqlSession sqlSession = BatisSessionFactory.getFactory().openSession();
        try{
            GenreMapper genreMapper = sqlSession.getMapper(GenreMapper.class);
            return genreMapper.getGenreByName(name);
        }finally {
            sqlSession.close();
        }
    }
    public List<Genre> getAllGenres(){
        SqlSession sqlSession = BatisSessionFactory.getFactory().openSession();
        try{
            GenreMapper genreMapper = sqlSession.getMapper(GenreMapper.class);
            return genreMapper.getAllGenres();
        }finally {
            sqlSession.close();
        }
    }
}
