package org.home.dao.service;

import org.apache.ibatis.session.SqlSession;
import org.home.dao.entity.User;
import org.home.dao.mappers.UserMapper;

import java.util.List;
import java.util.UUID;

public class UserService {
    public void insertUser(User user){
        //TODO
    }

    public User getUserByUserId(UUID userId){
        SqlSession sqlSession = BatisSessionFactory.getFactory().openSession();
        try{
            UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            return userMapper.getUserByUserId(userId);
        }finally {
            sqlSession.close();
        }
    }

    public User getUserByName(String name){
        SqlSession sqlSession = BatisSessionFactory.getFactory().openSession();
        try{
            UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            return userMapper.getUserByName(name);
        }finally {
            sqlSession.close();
        }
    }
    public User getUserByLogin(String login){
        SqlSession sqlSession = BatisSessionFactory.getFactory().openSession();
        try{
            UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            return userMapper.getUserByLogin(login);
        }finally {
            sqlSession.close();
        }
    }
    public List<User> getAllUsers(){
        SqlSession sqlSession = BatisSessionFactory.getFactory().openSession();
        try{
            UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            return userMapper.getAllUsers();
        }finally {
            sqlSession.close();
        }
    }
    public void updateUser(User user){
        //TODO
    }
    public void deleteUser(UUID userId){
        SqlSession sqlSession = BatisSessionFactory.getFactory().openSession();
        try{
            UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            userMapper.deleteUser(userId);
            sqlSession.commit();
        }finally {
            sqlSession.close();
        }
    }
}
