package org.home.home;

import org.home.model.*;
import java.security.Principal;
import org.springframework.core.SpringVersion;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Controller
class HomeController {

	@ModelAttribute("module")
	String module() {
		return "home";
	}

	@GetMapping("/")
	String index(Principal principal, SecurityContextHolder contextHolder, Model model) {
		model.addAttribute("springVersion", SpringVersion.getVersion());
		if(principal != null){
			LibraryResponse response = new LibraryData(contextHolder).getRangeBooks(5, 1);
			model.addAttribute("result", response);
			return "home/homeSignedIn";
		}
		return "home/homeNotSignedIn";
	}
}
