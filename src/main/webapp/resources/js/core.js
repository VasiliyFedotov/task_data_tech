$(document).ready(function () {
    //----------------delete book--------------
    $('.removeBook').on('click', function () {
        var rowIndex = $(this).attr('index');
        $('#lib tbody tr').each(function (index) {
            if (index == rowIndex) {
                $("#deleteBookModal .bookTitle").text($(this).find(".title").text());
                $("#deleteBookModal .bookId").val($(this).find(".id").text());
            }
        });
        $('#bookDeleteYes').on('click', function () {
            $('#bookDeleteYes').unbind("click");
            $.ajax({
                url: "lib/delete/" + $("#deleteBookModal .bookId").val(),
                type: "DELETE",
                cache: false
            }).done(function (data) {
                    updateTable(data);
                }
            );
            $('#deleteBookModal').modal('hide');
        });
        $("#deleteBookModal").modal('show');
    });

    //----------------view book--------------
    $('.viewBook').on('click', function () {
        var rowIndex = $(this).attr('index');
        $('#lib tbody tr').each(function (index) {
            if (index == rowIndex) {
                $("#viewBookModal .modal-body").find(".bookName").text($(this).find(".title").text());
                $("#viewBookModal .modal-body").find(".bookAuthors").text($(this).find(".authorGroup").text());
                $("#viewBookModal .modal-body").find(".bookGenre").text($(this).find(".genre").text());
                $("#viewBookModal .modal-body").find(".bookDesc").text($(this).find(".description").text());
            }
        });
        $("#viewBookModal").modal('show');
    });

    //----------------edit book--------------
    $('.editBook').on('click', function () {
        $('#editBookModal .modal-title').text('Edit Book');
        var rowIndex = $(this).attr('index');
        $('#lib tbody tr').each(function (index) {
            if (index == rowIndex) {
                initEditForm($(this).find(".id").text(),
                    $(this).find(".title").text(),
                    $(this).find(".description").text(),
                    $(this).find(".genreId").text(),
                    $(this).find(".authorsId").text());
            }
        });

        $('.saveEdit').on('click', function () {
            var data = readData();
            var tabIndex = $('.pagination li.active').text();
            $.ajax({
                url: "lib/edit/" + tabIndex,
                type: "POST",
                cache: false,
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(data),
                dataType: "json"
            }).done(function (data) {
                    updateTable(data);
                }
            );

            $('.saveEdit').unbind("click");
        });
        $(".saveEdit").text("Save");
        $("#editBookModal").modal('show');
    });

    //----------------add book--------------
    $('.addBook').on('click', function () {
        $('#editBookModal .modal-title').text('Add Book');
        initEditForm('', '', '', 1, 1);
        $('.saveEdit').on('click', function () {
            var data = readData();
            $.ajax({
                url: "lib/add",
                type: "POST",
                cache: false,
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(data),
                dataType: "json"
            }).done(function (data) {
                    updateTable(data);
                }
            );
            $('.saveEdit').unbind("click");
        });
        $(".saveEdit").text("Save");
        $("#editBookModal").modal('show');
    });

    //----------------search book--------------
    $('.searchBook').on('click', function () {
        $('#editBookModal .modal-title').text('Search Book');
        initEditForm('', '', '', 1, 1);
        $('.saveEdit').on('click', function () {
            var data = readData();
            $.ajax({
                url: "lib/search",
                type: "POST",
                cache: false,
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(data),
                dataType: "json"
            }).done(function (data) {
                    updateTable(data);
                }
            );
            $('.saveEdit').unbind("click");
        });
        $(".saveEdit").text("Search");
        $("#editBookModal").modal('show');
    });


    function readData() {
        return {
            id: $('.editBookForm').find('#id').val(),
            title: $('.editBookForm').find('#bookTitle').val(),
            description: $('.editBookForm').find('#deskText').val(),
            genreId: $('.editBookForm').find('#genreId').val(),
            authorGroupId: $('.editBookForm').find('#authorGroupId').val()
        };
    }

    function initEditForm(id, title, desc, genre, author) {
        $('.editBookForm').find('#id').val(id);
        $('.editBookForm').find('#bookTitle').val(title);
        setSelectOptions('genre', genre, 'genreId');
        setSelectOptions('authors', author, 'authorGroupId');
        $('.editBookForm').find('#deskText').val(desc);
    }

//------------update lib data after change tabPaginator--------------
    function dataRequest(page) {
        var requestURL ="lib/range/" + page;
        if($('#searchId').val() != '')
            requestURL = "lib/search/" + page +'/'+$('#searchId').val()
        $.ajax({
            url: requestURL,
            type: "POST",
            cache: false
        }).done(function (data) {
                updateTable(data);
            }
        );
    }

    var defaultPagOptions = {
        totalPages: $('#defaultPaginatorTotal').val(),
        initiateStartPageClick: false
    };

    $('#pagination').twbsPagination(defaultPagOptions)
        .on('page', function (event, page) {
            dataRequest(page);
        });

    function initPaginator(pActive, pTotal) {
        var pagination = $('#pagination');
        var pagOpts = {
            startPage: pActive,
            totalPages: pTotal
        };
        pagination.twbsPagination('destroy');
        pagination.twbsPagination($.extend({}, defaultPagOptions, pagOpts))
            .on('page', function (event, page) {
                dataRequest(page);
            });
    }

    function updateTable(data) {
        var library = data.library;
        $('#lib tbody tr').each(function (index) {
            if (index < library.length) {
                var value = library[index];
                $(this).removeClass('hideElement');
                $(this).find(".id").text(value.id);
                $(this).find(".title").text(value.title);
                $(this).find(".description").text(value.description);
                $(this).find(".genre").text(value.genre);
                $(this).find(".genreId").text(value.genreId);
                $(this).find(".authorsId").text(value.authorGroupId);
                $(this).find(".authorGroup").text(value.authorGroup);
            } else {
                $(this).addClass('hideElement');
            }
        });

        initPaginator(data.activePagingTab, data.totalPagingTab);
        $('#searchId').val(data.searchId);
    }

    function setSelectOptions(pathAlias, selectId, tagId) {
        $.ajax({
            url: "lib/" + pathAlias + "/" + selectId + "/" + tagId,
            type: "POST",
            cache: false
        }).done(function (data) {
            var actionId = data.actionId;
            var tagId = data.tagId;
            $('.editBookForm #' + tagId).empty();
            $.each(data.attributes, function (index, item) {
                $('.editBookForm #' + tagId)
                    .append($('<option>', {
                        value: item.id,
                        text: item.name
                    }));
            });
            $('.editBookForm #' + tagId + ' [value=' + actionId + ']').attr("selected", "selected");
        });
    }
});