package org.home.dao.service;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class CounterServiceTest {

    private static CounterService counterService;

    @BeforeClass
    public static void init(){
        counterService = new CounterService();
    }

    @AfterClass
    public static void teardown(){
        counterService = null;
    }

    @Test
    public void testGetTotalBooks(){
        Integer count = counterService.getTotalBooks();
        Assert.assertNotNull(count);
        Assert.assertEquals(31, count.intValue());
    }
}
