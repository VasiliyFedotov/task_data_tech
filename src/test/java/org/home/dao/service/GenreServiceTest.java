package org.home.dao.service;

import org.home.dao.entity.Genre;
import org.junit.*;

import java.util.List;

public class GenreServiceTest {

    private static GenreService genreService;

    @BeforeClass
    public static void init(){
        genreService = new GenreService();
    }

    @AfterClass
    public static void teardown(){
        genreService = null;
    }

    @Test
    public void testGetGenreById(){
        Genre genre = genreService.getGenreById(1);
        Assert.assertNotNull(genre);
        Assert.assertEquals("Юмор", genre.getName());
    }

    @Test
    public void testGetGenreByName(){
        Genre genre = genreService.getGenreByName("Детектив");
        Assert.assertNotNull(genre);
        Assert.assertEquals("Детектив", genre.getName());
        Assert.assertEquals(3, genre.getId().intValue());
    }

    @Test
    public void testGetAllGenres(){
        List<Genre> list = genreService.getAllGenres();
        Assert.assertNotNull(list);
        Assert.assertEquals(5, list.size());
        for (Genre genre : list) {
            System.out.println(String.format("Id : %d \t Name: %s ", genre.getId(), genre.getName()));
        }
    }
}
