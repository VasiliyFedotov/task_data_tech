package org.home.dao.service;

import org.home.dao.entity.User;
import org.home.utils.AccountRole;
import org.junit.*;

import java.util.List;
import java.util.UUID;

public class UserServiceTest {
    private static UserService userService;

    @BeforeClass
    public static void init(){
        userService = new UserService();
    }

    @AfterClass
    public static void teardown(){
        userService = null;
    }

    private void userEquals(User user, String name, int role){
        Assert.assertNotNull(user);
        Assert.assertEquals(name, user.getName());
        Assert.assertEquals(role, user.getRole().intValue());
    }
    @Test
    public void testGetUserByUserId(){
        UUID uuid = UUID.fromString("f39dcaca-1d71-417a-988b-a8896dcb8cc8");
        User user = userService.getUserByUserId(uuid);
        userEquals(user, "Vasiliy", 0);
    }

    @Test
    public void testGetUserByName(){
        String name = "Vasiliy";
        User user = userService.getUserByName(name);
        userEquals(user, name, 0);
    }

    @Test
    public void testGetUserByLogin(){
        String login = "Sergey";
        User user = userService.getUserByLogin(login);
        userEquals(user, login, 2);
        Assert.assertEquals("Sergey", user.getLogin());
    }

    @Test
    public void testGetAllUsers(){
        List<User> list = userService.getAllUsers();
        Assert.assertNotNull(list);
        Assert.assertEquals(3, list.size());
        for (User user : list) {
            AccountRole role = AccountRole.values()[user.getRole()];
            System.out.println(String.format("User : %s \t Login: %s \t Role: %s", user.getName(), user.getLogin(), role.name()));
        }
    }


}
