package org.home.dao.service;

import org.home.dao.entity.Author;
import org.junit.*;

import java.util.List;

public class AuthorServiceTest {
    private static AuthorService authorService;

    @BeforeClass
    public static void init(){
        authorService = new AuthorService();
    }

    @AfterClass
    public static void teardown(){
        authorService = null;
    }

    private void checkAuthor(Author author, String first, String second, String sur, boolean alias, Integer parent){
        Assert.assertNotNull(author);
        Assert.assertEquals(first, author.getFirstname());
        Assert.assertEquals(second, author.getSecondname());
        Assert.assertEquals(sur, author.getSurname());
        Assert.assertEquals(alias, author.getAlias());
        Assert.assertEquals(parent, author.getParentId());
    }

    @Test
    public void testGetAuthorById(){
        Author author = authorService.getAuthorById(9);
        checkAuthor(author, "Борис", "", "Акунин", true, 8);
        Assert.assertEquals(8, author.getGroupId().intValue());
    }
    @Test
    public void testGetAuthorByFirstname(){
        String name = "Леонид";
        List<Author> authors = authorService.getAuthorByFirstname(name);
        Assert.assertNotNull(authors);
        Assert.assertEquals(1, authors.size());
        checkAuthor(authors.get(0), name, "", "Филатов", false, null);
    }
    @Test
    public void testGetAuthorBySecondname(){
        String name = "Шалвович";
        List<Author> authors = authorService.getAuthorBySecondname(name);
        Assert.assertNotNull(authors);
        Assert.assertEquals(1, authors.size());
        checkAuthor(authors.get(0), "Григорий", name, "Чхартишвили", false, null);
    }
    @Test
    public void testGetAuthorBySurname(){
        String name = "Михайлов";
        List<Author> authors = authorService.getAuthorBySurname(name);
        Assert.assertNotNull(authors);
        Assert.assertEquals(2, authors.size());
        checkAuthor(authors.get(0), "Руслан", "Алексеевич", name, false, null);
        checkAuthor(authors.get(1), "Дем", "", name,true, 13);
    }
}
