package org.home.dao.service;

import org.home.dao.entity.Book;
import org.junit.*;

import java.util.List;

public class BookServiceTest {

    private static BookService bookService;

    @BeforeClass
    public static void init(){
        bookService = new BookService();
    }

    @AfterClass
    public static void teardown(){
        bookService = null;
    }

    private void bookEquals(Book book, String title, Integer genreId, Integer authorGroupId){
        Assert.assertNotNull(book);
        Assert.assertEquals(title, book.getTitle());
        Assert.assertEquals(genreId, book.getGenreId());
        Assert.assertEquals(authorGroupId, book.getAuthorGroupId());
    }
    @Test
    public void testGetBookById(){
        Book book = bookService.getBookById(1);
        bookEquals(book, "Двенадцать стульев", 1, 1);
    }

    @Test
    public void testGetAllBooks(){
        List<Book> books = bookService.getAllBooks();
        Assert.assertNotNull(books);
        Assert.assertEquals(31, books.size());
    }

    @Test
    public void testGetRangeBooks(){
        List<Book> books = bookService.getRangeBooks(5, 10);
        Assert.assertNotNull(books);
        Assert.assertEquals(5, books.size());
        bookEquals(books.get(2), "Карибская тайна", 3, 6);
    }

    @Test
    public void testGetBooksByRangeWithAuthor(){
        List<Book> books = bookService.getBooksByRangeWithAuthor(5, 0);
        Assert.assertNotNull(books);
        Assert.assertEquals(5, books.size());
        Book book = books.get(0);
        bookEquals(book, "Двенадцать стульев", 1, 1);
        String[] expected = new String[]{"Евгений Петров","Илья Ильф"};
        Assert.assertEquals(2, book.getAuthorGroup().size());
        Assert.assertArrayEquals(expected, book.getAuthorGroup().toArray());
    }
}
